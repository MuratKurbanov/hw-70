import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

export default class App extends React.Component {
  state = {
    screen: '',
    result: ''
  };

  getValue = (value) => {
    this.setState({
      result: '',
      screen: this.state.screen.concat(value)
    })
  };

  getResult = () => {
    const result = eval(this.state.screen);
    this.setState({
      screen: '',
      result: result
    })
  };

  remove = () => {
    this.setState({
      screen: this.state.screen.substr(0, this.state.screen.length - 1)
    })
  };

  delete = () => {
    this.setState({
      screen: '',
      result: ''
    })
  };

  render() {

    const buttons = ['C', '( )','%','/', '7', '8', '9','*','4', '5', '6', '-', '1', '2', '3', '+', '<', '0' , '.', '='];

    return (
        <View style={styles.container}>
          <View style={styles.screen}>
            <Text>{this.state.screen}</Text>
            <Text>{this.state.result}</Text>
          </View>
          <View style={styles.keypad}>

            {buttons.map((btn, index) => {
              if(btn === '=') {
                return (
                    <TouchableOpacity key={index} style={styles.btn} onPress={this.getResult}>
                      <Text>{btn}</Text>
                    </TouchableOpacity>
                )
              } else if (btn === '<') {
                return (
                    <TouchableOpacity key={index} style={styles.btn} onPress={this.remove}>
                      <Text>{btn}</Text>
                    </TouchableOpacity>
                )
              } else if (btn === 'C') {
                return (
                    <TouchableOpacity key={index} style={styles.btt} onPress={this.delete}>
                      <Text>{btn}</Text>
                    </TouchableOpacity>
                )
              } else {
                return (
                    <TouchableOpacity key={index} style={styles.btn}  onPress={() => this.getValue(btn)}>
                      <Text>{btn}</Text>
                    </TouchableOpacity>
                )
              }
            })}
          </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },

  screen: {
    width: '93.5%',
    marginLeft: 12,
    height: 200,
    alignItems: 'flex-end',
    paddingTop: 30,
    padding: 10
  },

  btn: {
    width: '24%',
    height: 80,
    margin: '0.1%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2684FF'
  },

  btt: {
    width: '24%',
    height: 80,
    margin: '0.1%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF272C'
  },

  keypad: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignSelf: 'center',
    paddingLeft: 12
  }
});
